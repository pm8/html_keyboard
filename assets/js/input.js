/**! @license input.js by YukiTANABE@PM8.JP | MIT License | PM8.jp */
var JP_PM8_GRAFKEY=JP_PM8_GRAFKEY||{};
JP_PM8_GRAFKEY.INPUT={
    funs:{
        init:()=>{
            navigator.requestMIDIAccess({sysex:true}).then(access=>{
                JP_PM8_GRAFKEY.INPUT.funs.listen(access);
            }, err=>{
                console.error(err);
            });
        },
        listen:(access)=>{
            let devices={
                inputs:{},
                outputs:{}
            };
            const inputs = access.inputs.values();
            for(let input=inputs.next(); !input.done; input=inputs.next()){
                let value=input.value;
                devices.inputs[value.name]=value;
                value.addEventListener('midimessage',e=>{
                    let n='0x'+e.data[0].toString(16).toUpperCase();
                    let m=e.data[1].toString(16);
                    if(!((parseInt(n,16)>>>4)^0x9)){
                        let t=document.querySelector('[data-number="'+m+'"]');
                        JP_PM8_GRAFKEY.OUTPUT.funs.pronunciation(t.getAttribute('data-cmp'));
                    }else{
                        JP_PM8_GRAFKEY.OUTPUT.funs.stop();
                    }
                },false)
            }
        }
    }
}