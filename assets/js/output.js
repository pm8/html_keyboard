/**! @license output.js by YukiTANABE@PM8.JP | MIT License | PM8.jp */
var JP_PM8_GRAFKEY=JP_PM8_GRAFKEY||{};
JP_PM8_GRAFKEY.OUTPUT={
    funs:{
        init:()=>{
            JP_PM8_GRAFKEY.OUTPUT.funs.listen();
        },
        listen:()=>{
            JP_PM8_GRAFKEY.COMMON.elems.keyboard.addEventListener('mousedown',e=>{
                let t=e.target;
                JP_PM8_GRAFKEY.OUTPUT.funs.pronunciation(t.getAttribute('data-cmp'))
            });
            JP_PM8_GRAFKEY.COMMON.elems.keyboard.addEventListener('mouseup',e=>{
                JP_PM8_GRAFKEY.OUTPUT.funs.stop();
            });
        },
        pronunciation:(cmp)=>{
            if(JP_PM8_GRAFKEY.COMMON.variables.isPlaying) return;
            JP_PM8_GRAFKEY.COMMON.oscillator = JP_PM8_GRAFKEY.COMMON.variables.ctx.createOscillator();
            JP_PM8_GRAFKEY.COMMON.oscillator.type = "sine"; // sine, square, sawtooth, triangle
            JP_PM8_GRAFKEY.COMMON.oscillator.frequency.setValueAtTime(cmp, JP_PM8_GRAFKEY.COMMON.variables.ctx.currentTime);
            JP_PM8_GRAFKEY.COMMON.oscillator.connect(JP_PM8_GRAFKEY.COMMON.variables.ctx.destination);
            JP_PM8_GRAFKEY.COMMON.oscillator.start();
            JP_PM8_GRAFKEY.COMMON.variables.isPlaying = true;
        },
        stop:()=>{
            JP_PM8_GRAFKEY.COMMON.oscillator.stop();
            JP_PM8_GRAFKEY.COMMON.variables.isPlaying=false;
        }
    }
}